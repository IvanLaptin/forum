﻿using Forum.DAL.Identity;
using Forum.DAL.Repositories;
using System;
using System.Threading.Tasks;

namespace Forum.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ApplicationUserManager UserManager { get; }
        IClientRepository ClientManager { get; }
        ApplicationRoleManager RoleManager { get; }
        ITopicsRepository TopicManager { get; }
        Task SaveAsync();
    }
}
