﻿using Forum.DAL.Entities;
using System;
using System.Threading.Tasks;

namespace Forum.DAL.Interfaces
{
    public interface IClientRepository : IDisposable
    {
        void Create(ClientProfile item);
        Task<ClientProfile> GetInfo(string id);
        Task ChangeInfo(ClientProfile newClientProfile);
    }
}
