﻿using Forum.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Forum.DAL.Interfaces
{
    public interface ITopicsRepository : IDisposable
    {
        Task Create(Topic item);
        Task<IEnumerable<Topic>> GetAllTopics();
        Task<Topic> GetTopicById(int id);
        Task<Topic> FindByNameAsync(string name);
        Task RemoveTopic(int id);
        Task<IEnumerable<Message>> GetMessagesInCurrentTopic(int id);
        Task SendMessageInCurrentTopic(int topicId, string authorId, string text);
        Task EditMessage(int messageId, string newStr);
        Task EditTopic(int topicId,string name,string description);
        Task DeleteMessage(int id);
    }
}
