﻿using Forum.DAL.Entities;
using Microsoft.AspNet.Identity;

namespace Forum.DAL.Identity
{
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store) : base(store)
        {

        }
    }
}
