﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Forum.DAL.Entities
{
    public class Message
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Topic")]
        public int TopicId { get; set; }

        [ForeignKey("ApplicationUser")]
        public string AuthorId { get; set; }

        public string Text { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual Topic Topic { get; set; }
    }
}
