﻿using System.ComponentModel.DataAnnotations;

namespace Forum.DAL.Entities
{
    public class Topic
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
