﻿using Forum.DAL.EF;
using Forum.DAL.Entities;
using Forum.DAL.Interfaces;
using System.Threading.Tasks;

namespace Forum.DAL.Repositories
{
    public class ClientRepository : IClientRepository
    {
        public ApplicationContext Database { get; set; }
        public ClientRepository(ApplicationContext db)
        {
            Database = db;
        }

        public void Create(ClientProfile item)
        {
            Database.ClientProfiles.Add(item);
            Database.SaveChanges();
        }

        public async Task<ClientProfile> GetInfo(string id)
        {
            return await Database.ClientProfiles.FindAsync(id);
        }

        public async Task ChangeInfo(ClientProfile newClientProfile)
        {
            var client = await Database.ClientProfiles.FindAsync(newClientProfile.Id);
            client.Name = newClientProfile.Name;
            client.Address = newClientProfile.Address;
            await Database.SaveChangesAsync();
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
