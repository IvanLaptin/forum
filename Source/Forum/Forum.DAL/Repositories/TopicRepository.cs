﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Forum.DAL.EF;
using Forum.DAL.Entities;
using Forum.DAL.Interfaces;

namespace Forum.DAL.Repositories
{
    public class TopicRepository : ITopicsRepository
    {
        public ApplicationContext Database { get; set; }

        public TopicRepository(ApplicationContext db)
        {
            Database = db;
        }

        public async Task Create(Topic item)
        {
            Database.Topics.Add(item);
            await Database.SaveChangesAsync();
        }

        public async Task<IEnumerable<Topic>> GetAllTopics()
        {
            return await Database.Topics.ToListAsync();
        }

        public void Dispose()
        {
            Database.Dispose();
        }

        public async Task<Topic> FindByNameAsync(string name)
        {
            var res = await Database.Topics.ToListAsync();
            return res.FirstOrDefault(t => t.Name == name);
        }

        public async Task RemoveTopic(int id)
        {
            var t = await Database.Topics.FindAsync(id);
            if(t != null)
            {
                Database.Topics.Remove(t);
            }
            await Database.SaveChangesAsync();
        }

        public async Task SendMessageInCurrentTopic(int topicId,string authorId,string text)
        {
            Database.Messages.Add(new Message() { TopicId = topicId, AuthorId = authorId, Text = text });
            await Database.SaveChangesAsync();
        }

        public async Task<IEnumerable<Message>> GetMessagesInCurrentTopic(int topicId)
        {
            var t = await Database.Topics.FindAsync(topicId);
            if (t != null)
            {
                var res = await Database.Messages.Where(x => x.TopicId == topicId).Include(x=> x.ApplicationUser).ToListAsync();
                return res;
            }
            return null;
        }

        public async Task EditMessage(int messageId, string newStr)
        {
            var message = await Database.Messages.FindAsync(messageId);
            message.Text = newStr;
            await Database.SaveChangesAsync();
        }

        public async Task<Topic> GetTopicById(int id)
        {
            var topic = await Database.Topics.FindAsync(id);
            return topic;
        }

        public async Task EditTopic(int topicId, string name, string description)
        {
            var topic = await Database.Topics.FindAsync(topicId);
            topic.Name = name;
            topic.Description = description;
            await Database.SaveChangesAsync();
        }

        public async Task DeleteMessage(int id)
        {
            var messages = await Database.Messages.ToListAsync();
            var message = messages.FirstOrDefault(x => x.Id == id);
            if(message != null)
            {
                 Database.Messages.Remove(message);
                await Database.SaveChangesAsync();
            }
            
        }
    }
}
