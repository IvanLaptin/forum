﻿using Forum.BLL.DTO;
using Forum.BLL.Infrastructure;
using Forum.BLL.Interfaces;
using Forum.DAL.Entities;
using Forum.DAL.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.DataProtection;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;
using AutoMapper;
using System.Linq;

namespace Forum.BLL.Services
{
    public class UserService : IUserService
    {
        IUnitOfWork Database { get; set; }

        private string urlSite { get; set; }

        EmailService emailService;

        public UserService(IUnitOfWork uow)
        {
            Database = uow;
            emailService = new EmailService();
            var provider = new DpapiDataProtectionProvider("Forum.WEB");
            Database.UserManager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(provider.Create("EmailConfirmation"));
            InitAutoMapper();
        }

        private void InitAutoMapper()
        {
            Mapper.Initialize(cfg => {
                cfg.CreateMap<Topic, TopicDTO>();
                cfg.CreateMap<Message, MessageDTO>()
                .ForMember("User", opt => opt.MapFrom(src => new UserDTO()
                {
                    Id = src.ApplicationUser.Id,
                    Email = src.ApplicationUser.Email,
                    Name = src.ApplicationUser.UserName
                }));
            });
        }

        public async Task<OperationDetails> Create(UserDTO userDto)
        {
            ApplicationUser user = await Database.UserManager.FindByEmailAsync(userDto.Email);
            if (user == null)
            {
                user = new ApplicationUser { Email = userDto.Email, UserName = userDto.Email };
                var res = await Database.UserManager.CreateAsync(user, userDto.Password);
                if(res.Succeeded)
                {
                    await Database.UserManager.AddToRoleAsync(user.Id, userDto.Role);
                    ClientProfile clientProfile = new ClientProfile { Id = user.Id, Address = userDto.Address, Name = userDto.Name };
                    Database.ClientManager.Create(clientProfile);
                    await Database.SaveAsync();
                    await SendConfirmationByMail(user.Id, user.Email);
                    return new OperationDetails(true, "Регистрация успешно пройдена", "");
                }
               else
               {
                    string errorsStr = "";
                    foreach (var error in res.Errors)
                    {
                        errorsStr += error;
                    }
                    return new OperationDetails(false, errorsStr, "Email");
               }

            }
            else
            {
                return new OperationDetails(false, "Пользователь с таким логином уже существует", "Email");
            }
        }

        public void SetUrlThisSite(string url)
        {
            urlSite = url;   
        }

        private async Task SendConfirmationByMail(string id,string email)
        {
            var code = await Database.UserManager.GenerateEmailConfirmationTokenAsync(id);
            var callbackUrl = urlSite + "Account/ConfirmEmail?userId=" + id + "&code=" + code;
            var str = "Для завершения регистрации перейдите по ссылке <a href=\""
                                     + callbackUrl + "\">завершить регистрацию</a>";
            await emailService.SendAsync2(email, new IdentityMessage() { Body = str, Destination = "Подтверждение электронной почты", Subject = "Подтверждение электронной почты" });
        }

        public async Task<OperationDetails> ConfirmEmail(string userId, string code)
        {
            var changeCode = code.Replace(" ", "+");
            var res = await Database.UserManager.ConfirmEmailAsync(userId, changeCode);
            if(res.Succeeded)
            {
                return new OperationDetails(true,"Успешно подтверждена почта","");
            }
            else
            {
                return new OperationDetails(false, "Неверные данные", "");
            }
        }

        public async Task<UserDTO> GetUserInfo(string email)
        {
            var user = await Database.UserManager.FindByEmailAsync(email);
            if(user != null)
            {
                var profile = await Database.ClientManager.GetInfo(user.Id);
                return new UserDTO
                {
                    Id = user.Id,
                    Email = user.Email,
                    Name = profile.Name,
                    Address = profile.Address,
                    EmailConfirmed = user.EmailConfirmed
                };
            }
            return null;
        }

        public async Task<ClientProfile> GetClientInfo(string id)
        {
            var res = await Database.ClientManager.GetInfo(id);
            return res;
        }

        public async Task<ClaimsIdentity> Authenticate(UserDTO userDto)
        {
            ClaimsIdentity claim = null;
            ApplicationUser user = await Database.UserManager.FindAsync(userDto.Email, userDto.Password);
            if (user != null)
                claim = await Database.UserManager.CreateIdentityAsync(user,
                                            DefaultAuthenticationTypes.ApplicationCookie);
            return claim;
        }

      
        public async Task SetInitialData(UserDTO adminDto, List<string> roles)
        {
            foreach (string roleName in roles)
            {
                var role = await Database.RoleManager.FindByNameAsync(roleName);
                if (role == null)
                {
                    role = new ApplicationRole { Name = roleName };
                    await Database.RoleManager.CreateAsync(role);
                }
            }

            await Create(adminDto);
        }

        public void Dispose()
        {
            Database.Dispose();
        }
        
        public async Task<OperationDetails> CreateTopic(DTO.TopicDTO topic)
        {
            Topic t = await Database.TopicManager.FindByNameAsync(topic.Name);
            if(t == null)
            {
                 await Database.TopicManager.Create(new Topic() { Name = topic.Name, Description = topic.Description });
                 return new OperationDetails(true, "Topic added", "");
            }
            return new OperationDetails(false, "Topic already exists", "");
        }

        public async Task<IEnumerable<TopicDTO>> GetAllTopics()
        {
            var res = await Database.TopicManager.GetAllTopics();
            return Mapper.Map<IEnumerable<Topic>, IEnumerable<TopicDTO>>(res);
        }

        public async Task<TopicDTO> GetTopicById(int id)
        {
            var res = await Database.TopicManager.GetTopicById(id);
            return Mapper.Map<Topic, TopicDTO>(res);
        }

        public async Task RemoveTopic(int id)
        {
            await Database.TopicManager.RemoveTopic(id);
        }

        public async Task<OperationDetails> EditTopic(int topicId, string name, string description)
        {
            var topic = await Database.TopicManager.GetTopicById(topicId);
            var allTopics = await Database.TopicManager.GetAllTopics() as List<Topic>;
            var res = allTopics.Where(t => t.Name == name && t.Id != topicId);
            if(topic != null && res.Count() == 0)
            {
                await Database.TopicManager.EditTopic(topicId, name, description);
                return new OperationDetails(true, "", "");
            }
            return new OperationDetails(false, "Topic already exists", "");
        }

        public async Task<IEnumerable<MessageDTO>> GetMessagesInTopic(int id)
        {
            var res = await Database.TopicManager.GetMessagesInCurrentTopic(id);
            return Mapper.Map<IEnumerable<Message>, IEnumerable<MessageDTO>>(res);
        }

        public async Task SendMessageInTopic(int topicId, string authorEmail, string text)
        {
            var author = await GetUserInfo(authorEmail);
            await Database.TopicManager.SendMessageInCurrentTopic(topicId, author.Id, text);
        }

        public async Task EditMessage(int messageId, string newMessage)
        {
            await Database.TopicManager.EditMessage(messageId, newMessage);
        }

        public async Task DeleteMessage(int id)
        {
            await Database.TopicManager.DeleteMessage(id);
        }
    }
}
