﻿using Forum.DAL.Interfaces;
using Forum.DAL.Repositories;
using Ninject.Modules;

namespace Forum.BLL.Services
{
    public class ServiceModule : NinjectModule
    {
        private string connectionString;
        public ServiceModule(string connection)
        {
            connectionString = connection;
        }
        public override void Load()
        {
            Bind<IUnitOfWork>().To<IdentityUnitOfWork>().WithConstructorArgument(connectionString);
        }
    }
}
