﻿using Forum.BLL.DTO;
using Forum.BLL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Forum.BLL.Interfaces
{
    public interface IUserService : IDisposable
    {
        Task<OperationDetails> Create(UserDTO userDto);
        Task<OperationDetails> ConfirmEmail(string userId, string code);
        void SetUrlThisSite(string url);
        Task<ClaimsIdentity> Authenticate(UserDTO userDto);
        Task SetInitialData(UserDTO adminDto, List<string> roles);
        Task<UserDTO> GetUserInfo(string email);
        Task<OperationDetails> CreateTopic(DTO.TopicDTO topic);
        Task<IEnumerable<TopicDTO>> GetAllTopics();
        Task<TopicDTO> GetTopicById(int id);
        Task RemoveTopic(int id);
        Task<IEnumerable<MessageDTO>> GetMessagesInTopic(int id);
        Task SendMessageInTopic(int topicId, string authorEmail,string text);
        Task EditMessage(int messageId,string newMessage);
        Task<OperationDetails> EditTopic(int topicId, string name, string description);
        Task DeleteMessage(int id);
    }
}
