﻿namespace Forum.BLL.DTO
{
    public class MessageDTO
    {
        public int Id { get; set; }
        public int TopicId { get; set; }
        public string AuthorId { get; set; }
        public string Text { get; set; }
        public UserDTO User { get; set; }
    }
}
