﻿namespace Forum.WEB.Models.Topic
{
    public class ResponseOnAddNewTopic
    {
        public bool Result { get; set; }
        public string mainError { get; set; }
        public string errorName { get; set; }
        public string errorDesription { get; set; }
    }
}