﻿namespace Forum.WEB.Models.Topic
{
    public class ResponseOnSendMessage
    {
        public bool Result { get; set; }
        public string Error { get; set; }
    }
}