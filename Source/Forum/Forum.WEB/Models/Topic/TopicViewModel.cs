﻿using Forum.BLL.DTO;
using System.Collections.Generic;

namespace Forum.WEB.Models.Topic
{
    public class TopicViewModel
    {
        public TopicDTO Topic { get; set; }
        public IEnumerable<MessageDTO> Messages { get; set; }
    }
}