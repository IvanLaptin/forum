﻿
namespace Forum.WEB.Models.Topic
{
    public class ResponseOnEditTopicData
    {
        public bool Result { get; set; }
        public string ErrorName { get; set; }
        public string ErrorDescription { get; set; }
        public string MainError { get; set; }
    }
}