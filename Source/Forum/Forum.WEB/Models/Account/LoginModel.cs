﻿using System.ComponentModel.DataAnnotations;

namespace Forum.WEB.Models.Account
{
    public class LoginModel
    {
        [Required]
        public string Email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}