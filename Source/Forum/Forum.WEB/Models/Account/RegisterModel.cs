﻿using System.ComponentModel.DataAnnotations;

namespace Forum.WEB.Models.Account
{
    public class RegisterModel
    {
        [Required]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Invalid email")]
        public string Email { get; set; }
        [Required]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,15}$", ErrorMessage = "The password must contain at least 7 characters, have one upper case character, lower case character, number")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
        [Required]
        [RegularExpression(@"^(?=.*[A-zА-я0-9]).{8,15}$", ErrorMessage = "The address must contain at least 8 characters")]
        public string Address { get; set; }
        [Required]
        [RegularExpression(@"^(?=.*[A-zА-я]).{4,15}$", ErrorMessage = "The name must contain at least 4 characters")]
        public string Name { get; set; }
    }
}