﻿var UrlForEditMessage;
var UrlForSendMessage;
var UrlForDeleteMessage;

$(document).ready(function ()
{
   
    $('#errSendMessage').hide();
    $('#editPanel').hide();
    $('#errEditMessage').hide();

    $('#BtnEditMessage').click(function () {
        $.ajax({
            type: "POST",
            url: UrlForEditMessage,
            data:
                {
                    idMessage: $("#idMessageHideLabel").text(),
                    newMessage: $('#valueForEdit').val()
                }
        }).done(function (msg) {
            if (msg.Result) {
                $('#errEditMessage').text("");
                location.reload(true);
            }
            else {
                $('#errEditMessage').show();
                $('#errEditMessage').text(msg.Error);
            }   
        });
    });

    

});

function SetUrlForEditMessage(urlForEditMessage) {
    UrlForEditMessage = urlForEditMessage;
}

function SetUrlForSemdMessage(urlForSendMessage) {
    UrlForSendMessage = urlForSendMessage;
}


function SetUrlForDeleteMessage(urlForDeleteMessage) {
    UrlForDeleteMessage = urlForDeleteMessage;
}

function EditMessage(id, text) {
    $('#errEditMessage').text("");
    $('#exampleModal').modal('show');
    $('#valueForEdit').val(text);
    $("#idMessageHideLabel").text(id);
}

function DeleteMessage(id) {
    $.ajax({
        type: "POST",
        url: UrlForDeleteMessage,
        data:
        {
            messageId: id
        }
    }).done(function () {
        location.reload(true);
    });
}

function SendMessage(id) {
    $.ajax({
        type: "POST",
        url: UrlForSendMessage,
        data:
            {
                text: $('#InpText').val(),
                topicId: id
            }
    }).done(function (msg) {
        if (msg.Result) {
            $('#errSendMessage').val("");
            location.reload(true);
        }
        else {
            $('#errSendMessage').show();
            $('#errSendMessage').text(msg.Error);
        }   
    });
}


