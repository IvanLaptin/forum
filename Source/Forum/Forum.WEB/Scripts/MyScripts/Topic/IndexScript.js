﻿var UrlForAddTopic;
var UrlForRemoveTopic;
var UrlForEditTopic;

$(document).ready(function () {
    $('#errNewTopicName').hide();
    $('#errNewTopicDesription').hide();
    $('#errNewTopicMain').hide();

    $('#BtnAddTopic').click(function () {
        $.ajax({
            type: "POST",
            url: UrlForAddTopic,
            data:
            {
                name: $('#InpNameTopic').val(),
                description: $('#InpDescriptionTopic').val()
            }
        }).done(function (msg) {
            $('#errNewTopicName').text("");
            $('#errNewTopicDesription').text("");
            $('#errNewTopicMain').text("");
            $('#errNewTopicName').hide();
            $('#errNewTopicDesription').hide();
            $('#errNewTopicMain').hide();
            if (msg.Result) {
                location.reload(true);
            } else {
                $('#errNewTopicName').show();
                $('#errNewTopicDesription').show();
                $('#errNewTopicMain').show();
                $('#errNewTopicName').text(msg.errorName);
                $('#errNewTopicDesription').text(msg.errorDesription);
                $('#errNewTopicMain').text(msg.mainError);

            }
        });
    });

    $("#lblErrorTopicName").text("");
    $("#lblErrorTopicDesciprion").text("");
    $("#lblErrorTopicMain").text("");

    $('#BtnSaveEditTopic').click(function () {
        $.ajax({
            type: "POST",
            url: UrlForEditTopic,
            data:
            {
                id: $('#inpEditTopicId').val(),
                name: $('#inpEditTopicName').val(),
                description: $('#inpEditTopicDescription').val()
            }
        }).done(function (msg) {
            if (msg.Result) {
                location.reload(true);
            } else {
                $("#lblErrorTopicName").text(msg.ErrorName);
                $("#lblErrorTopicDesciprion").text(msg.ErrorDescription);
                $("#lblErrorTopicMain").text(msg.MainError);
            }
        });
    });

});
    

function EditTopic(id, name, description) {
    $("#lblErrorTopicName").text("");
    $("#lblErrorTopicDesciprion").text("");
    $("#lblErrorTopicMain").text("");

    $('#exampleModal').modal('show');
    $('#inpEditTopicId').val(id);
    $('#inpEditTopicName').val(name);
    $('#inpEditTopicDescription').val(description);
}


function SetUrlForAddTopic(urlForAddTopic) {
    UrlForAddTopic = urlForAddTopic;
}

function SetUrlForRemoveTopic(urlForRemoveTopic) {
    UrlForRemoveTopic = urlForRemoveTopic;
}

function SetUrlForEditTopic(urlForEditTopic) {
    UrlForEditTopic = urlForEditTopic;
}

function RemoveTopic(id) {
    $.ajax({
        type: "POST",
        url: UrlForRemoveTopic,
        data:
            {
                id: id
            }
    }).done(function () {
        location.reload(true);
    });
}
    




