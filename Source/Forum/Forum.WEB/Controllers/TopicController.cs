﻿using Forum.BLL.Interfaces;
using Forum.WEB.Models.Topic;
using Microsoft.Owin.Security;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Forum.WEB.Controllers
{
    public class TopicController : Controller
    {
        private IUserService UserService;

        public TopicController(IUserService serv)
        {
            UserService = serv;
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public async Task<ActionResult> Index()
        {
            var res = await UserService.GetAllTopics();
            return View(res);
        }

        [HttpPost]
        public async Task<JsonResult> AddTopic(string name,string description)
        {
            var resValidName = CheckValidateString(name, new Regex(@"^(?=.*[А-яA-z0-9]).{4,15}$"));
            var resValidDescription = CheckValidateString(description, new Regex(@"^(?=.*[А-яA-z0-9]).{8,40}$"));
            if(resValidName && resValidDescription)
            {
                var res = await UserService.CreateTopic(new BLL.DTO.TopicDTO { Name = name, Description = description });
                if (res.Succedeed)
                {
                    return Json(new ResponseOnAddNewTopic { Result = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new ResponseOnAddNewTopic { Result = false, mainError = res.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                 var r = new ResponseOnAddNewTopic
                 {
                     Result = false,
                     errorName = resValidName ? "" : "Name must contain at least 4 characters and less than 15",
                     errorDesription = resValidDescription ? "" : "Description must contain at least 8 characters and less than 40"
                 };
                return Json(r, JsonRequestBehavior.AllowGet); 
            }
        }

        public bool CheckValidateString(string str,Regex reg)
        {
            return reg.IsMatch(str);
        }
        

        [HttpPost]
        public async Task RemoveTopic(int id)
        {
            await UserService.RemoveTopic(id);
        }

        [HttpPost]
        public async Task<JsonResult> EditTopicData(int id,string name,string description)
        {
            var resValidName = CheckValidateString(name, new Regex(@"^(?=.*[А-яA-z0-9]).{4,15}$"));
            var resValidDescription = CheckValidateString(description, new Regex(@"^(?=.*[А-яA-z0-9]).{8,15}$"));
            if (resValidName && resValidDescription)
            {
                var res = await UserService.EditTopic(id,name,description);
                if (res.Succedeed)
                {
                    return Json(new ResponseOnEditTopicData { Result = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new ResponseOnEditTopicData { Result = false, MainError = res.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                var r = new ResponseOnEditTopicData
                {
                    Result = false,
                    ErrorName = resValidName ? "" : "The name must contain at least 4 characters",
                    ErrorDescription = resValidDescription ? "" : "The description must contain at least 8 characters"
                };
                return Json(r, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task DeleteMessage(int messageId)
        {
            await UserService.DeleteMessage(messageId);
        }

        [Authorize]
        public async Task<ActionResult> TopicView(int id)
        {
            var message = await UserService.GetMessagesInTopic(id);
            var topic = await UserService.GetTopicById(id);
            return View(new TopicViewModel() { Messages = message, Topic = topic });
        }

        [HttpPost]
        public async Task<JsonResult> SendMessage(string text,int topicId)
        {
            if(CheckValidateString(text, new Regex(@"^(?=.*[А-яA-z0-9]).{1,65}$")))
            {
                await UserService.SendMessageInTopic(topicId, AuthenticationManager.User.Identity.Name, text);
                return Json(new ResponseOnSendMessage { Result = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new ResponseOnSendMessage { Result = false, Error = "The message must be more than one character and less than 65 chars." }, JsonRequestBehavior.AllowGet);
            }
            
        }

        [HttpPost]
        public async Task<JsonResult> EditMessage(string idMessage, string newMessage)
        {
            if (CheckValidateString(newMessage, new Regex(@"^(?=.*[А-яA-z0-9]).{1,45}$")))
            {
                await UserService.EditMessage(int.Parse(idMessage), newMessage);
                return Json(new ResponseOnSendMessage { Result = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new ResponseOnSendMessage { Result = false, Error = "The message must be more than one character and less than 30 chars." }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}