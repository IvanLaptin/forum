﻿using Forum.BLL.Interfaces;
using Forum.BLL.Services;
using Ninject.Modules;

namespace Forum.WEB.Utils
{
    public class UserModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserService>().To<UserService>().InSingletonScope();
        }
    }
}